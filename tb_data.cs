using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

/*本控件适用于嵌套在完整表格<table>中，表格线粗细决定于该表格
 * 控件插入位置为
 <table>
 <tr><td>1</td><td>2</td>...</tr>
 <tr><td>1</td><td>2</td>...</tr>
   该控件
   其后不能再有行<tr>
 </table> 
*/

/*
属性设置：（**为必填项目）
1、Dtbname：       存储数据的table表名**，只能从后台赋值
2、linkorNo：      是否在表格中加链接
3、PageSize：      每页记录数
4、DispPageCount： 每次显示页码数
5、Trbgcolor：     表内容背景参数
6、Tdheight：      表格行高度
7、nag_Trbgcolor： 导航栏背景参数
8、Columnum：     表格显示列数，与所在表格的第一行（表头）列数必须一致**

9、LinkType：在LinkorNo属性为true时必须设置的属性：
   设定链接打开方式，t1是指定参数方法打开，t2是直接打开指定页，常用于下载--*、

（1）Dt_guid：     t1方式是存储数据的table表的关键（具有唯一性）字段；t2方式是存储页面（要下载文件）相对路径的字段
                   t2方式示例：
				   select doc_name,doc_type1,doc_enddate,'http://localhost/dzs/inteinfo/e_docu/'+file_location dtpage from ken_edocument
				   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid    用于传递参数的变量的名称
（3）Linkpage      接收参数的页面 
11、在ChborNo属性为true（加入复选框）时必须设置的属性
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）Delepage：    执行删除功能的页面
（4）此时表头第一列用于对应checkbox，表头因此应该比 Columnum多加一列

-----可以加六列附加列，单数为在当前窗口打开，双数为新开窗口-----
12、LinkAdd1  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加一列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La1Text   ：  表格中显示的文字
（4）La1Page   ：  连接到的页面
13、LinkAdd2  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加二列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La2Text   ：  表格中显示的文字
（4）La2Page   ：  连接到的页面
14、LinkAdd3  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加三列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La3Text   ：  表格中显示的文字
（4）La3Page   ：  连接到的页面
15、LinkAdd4  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加四列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La4Text   ：  表格中显示的文字
（4）La4Page   ：  连接到的页面
16、LinkAdd5  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加五列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La5Text   ：  表格中显示的文字
（4）La5Page   ：  连接到的页面
17、LinkAdd6  在有附加连接时必须设置的属性值，所以表头应该比 Columnum多加六列
（1）Dt_guid：     存储数据的table表的关键（具有唯一性）字段；
                   要设置该属性则必须在sql语句中关键字段选上；不指定则出错**
（2）Trans_guid：  用于传递参数的变量的名称
（3）La6Text   ：  表格中显示的文字
（4）La6Page   ：  连接到的页面
----------------------------------------------------------------------------------------
 注：删除方法举例
        private void Page_Load(object sender, System.EventArgs e)
		{
			Label1.Text="正在删除........";
			string deleid=Request.QueryString["Trans_guid"];
			string[] array_del= deleid.Split(',');
			for(int i=0;i<array_del.Length;i++)
			{
				SqlDataAdapter dzs_kenAdap = new SqlDataAdapter("select * from ken_info where id='"+array_del[i]+"'", dzs_kenConn);
				SqlCommandBuilder ken_cb = new SqlCommandBuilder(dzs_kenAdap);
				dzs_kenAdap.Fill(ds, "ken_info");
				ken_info = ds.Tables["ken_info"];
				//如果收件人没有删除该信息
				if(ken_info.Rows[0]["recipients"].ToString().Split('/').Length<=1)
					ken_info.Rows[0]["sender"]+="/*";
				else
					ken_info.Rows[0].Delete();

				dzs_kenAdap.Update(ds,"ken_info");
				

			}
		dzs_kenConn.Close();
		
		Response.Redirect("sendinfo.aspx");
		
其他：
1、必须将控件所在表格包含在<form runat="server"></form>中，特别注意如果有iframe，不能嵌套Form
2、显示内容依次为sql语句中的前Columnum个字段
3、如果要使用保存为excel功能，须在指定临时目录属性，并设置读写权限
   属性名Dir_xls
   上级目录用..//temp//
   本级目录用temp//
4、保存为excel时的列数取决于sql语句中选择的字段数，而跟显示在页面的表格列数无关
5、保存为excel时使用汉字表头，在Th_xls属性中设置，以#分开，并且必须等于sql语句中选择的字段数

------------------------------------------**如果分页控件和显示内容在同一页面**------------------------------------------------------

*必须设定的属性：
Trans_page    用于传递页码的变量的名称

需加入的代码：
（1） 在显示指定记录时，记住当前页码，如果不需要在切换页码时，每页都显示该页第一条记录，只用该代码即可
在page_load中加入：
if(!Page.IsPostBack)
{
	if(Request.QueryString["pageNo"]!=null&&Request.QueryString["pageNo"]!="")
	{
		Tbpage1.CurrentTab=(int)((Convert.ToInt16(Request.QueryString["pageNo"])-1)/Tbpage1.DispPageCount);
		Tbpage1.CurrentPageIndex=Convert.ToInt16(Request.QueryString["pageNo"])%Tbpage1.DispPageCount==0?Tbpage1.DispPageCount:Convert.ToInt16(Request.QueryString["pageNo"])%Tbpage1.DispPageCount;
		//Page.RegisterStartupScript("tt","<script>alert('"+Tbpage1.CurrentPageIndex+"')</script>");
	}

}

（2）在切换页码时，每页都显示该页第一条记录
还同时应在Page_PreLoad加入以下代码：
private void Page_PreLoad(object sender, System.EventArgs e)
{
	string newsid=ken_news_tb.Rows[Tbpage1.nStartcount]["newsid"].ToString();
	SqlDataAdapter dzs_kenAdap = new SqlDataAdapter("select * from ken_news where newsid='"+newsid+"'", dzs_kenConn);
	dzs_kenAdap.Fill(ds, "ken_news");
	ken_news = ds.Tables["ken_news"];
	//Page.RegisterStartupScript("tt","<script>alert('"+Tbpage1.nStartcount+"')</script>");
	
}

private void InitializeComponent()
{    
	this.Load += new System.EventHandler(this.Page_Load);
	this.PreRender+=new System.EventHandler(this.Page_PreLoad);
}


--------------------------------------------------------**如果有查询条件，则需用session**------------------------------------------
 protected string str_se="1=1";
	

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			if(Session["str_select"]!=null&&Session["str_select"].ToString()!="")
				str_se=Session["str_select"].ToString();

			SqlDataAdapter ken_picAdap =new SqlDataAdapter("select filename,picclass,picof,convert(varchar(10),picmaketime,120) picmakedate,convert(varchar(6),picwidth)+'x'+convert(varchar(6),picheight) picmesure,convert(int,filesize/1024) filesize_kb,picuploadperson,ifpole=case  when havepole='1' then '√' else null end  ,atid from ken_pic where "+str_se+"order by picuploadtime desc",dzs_kenConn);
			
			ken_picAdap.Fill(ds,"ken_pic");

			ken_pic=ds.Tables["ken_pic"];

			Tbpage1.Dtbname=ken_pic;

			//Response.Write(str_se);
		

				
		}

        private void Button1_Click(object sender, System.EventArgs e)
		{
			string strCon="1=1";

			if(Request.Form["T1"].ToString().Trim()==""&&Request.Form["T2"].ToString()==""&&Request.Form["T3"].ToString()==""&&Request.Form["pic_class"].ToString()==""&&Request.Form["pic_of"].ToString()=="")
			{
				Response.Write("<script>alert('请输入查询条件！')</script>");
			}
			else
			{
				if(Request.Form["T1"].ToString().Trim()!="")
					strCon="filename like '%"+Request.Form["T1"].ToString()+"%'";
				if(Request.Form["pic_class"].ToString()!="")
					strCon=strCon+" and "+"picclass = '"+Request.Form["pic_class"].ToString()+"'";
                if(Request.Form["pic_of"].ToString()!="")
					strCon=strCon+" and "+"picof = '"+Request.Form["pic_of"].ToString()+"'";
				if(Request.Form["T2"].ToString()!=""&&Request.Form["T3"].ToString()!="")
					strCon=strCon+" and "+"picmaketime>='"+Request.Form["T2"].ToString()+"' and picmaketime<='"+Request.Form["T3"].ToString()+"'";

     		}
			
			Session["str_select"]=strCon;
			Response.Redirect("picview.aspx");

*/
namespace tb_data
{
	
	

	[DefaultProperty("Columnum"),
	ToolboxData("<{0}:tbpage runat=server></{0}:tbpage>")]
	public class tbpage : System.Web.UI.WebControls.WebControl,INamingContainer
	{
		//public delegate Object GetMyDataSource();
		//public GetMyDataSource gmds;
	

		//DataTable ken_tb;

		int nTotalPage = 0;
		int nEndPage;
        int nTotalCount;
		TableCell nagcell;

		//string[] thd;
		//LinkButton saveasxls;
		//TableCell boxcell;
		//TableRow trcontent;
		//TableRow nagrow;

		LinkButton[] GoPage;
        //LinkButton lbPrev;
        //LinkButton lbNext;
       
        LinkButton lbNextn;
		LinkButton lbPrevn;
		
		//CheckBox cbevery,cbsele;
		//int paranstart=1;
		
		
		#region excel相关参数
		/*
		private string dir_xls;
		[Bindable(true),
		Category("excel相关"),
		DefaultValue("")]
		
		public string Dir_xls
		{
			get
			{
				//对于不在Render等出现的属性，注意判断为空现象
				if(dir_xls==null)	
					return "temp/";
				else
					return dir_xls;
			
			}

			set
			{
				dir_xls = value;
			}
		}

		private string th_xls;
		[Bindable(true),
		Category("excel相关"),
		DefaultValue(" ")]
		
		public string Th_xls
		{
			get
			{
				//对于不在Render等出现的属性，注意判断为空现象
				if(th_xls==null)	
				     return "";
				else
					return th_xls;
			
			}

			set
			{
				th_xls = value;
			}
		}*/
		#endregion excel相关参数
		
		#region 表格数据内容属性
		//表名参数
		private DataTable dtbname;
		public DataTable Dtbname
		{
			get
			{
				
				return dtbname;
			}
			set
			{
				dtbname = value;
			}
		}



		//每页记录数
		//private int pageSize;
		[Bindable(true),
		Category("data"),
		DefaultValue(10)]
		
		public int PageSize
		{
			get
			{
				if (ViewState["PageSize"] == null)
					return 10;
				return (int)ViewState["PageSize"];
			
			}
			set
			{
				ViewState["PageSize"] = value;				
			}
		}
		//每次显示页码数
		//private int dispPageCount;
		[Bindable(true),
		Category("data"),
		DefaultValue("10")]
		
		public int DispPageCount
		{
			
			
			get
			{
				if (ViewState["DispPageCount"] == null)
					return 10;
				return (int)ViewState["DispPageCount"];
			}
			set
			{
				ViewState["DispPageCount"] = value;
			}
			
			
		}

		

		//关键字段
		private string dt_guid;
		[Bindable(true),
		Category("data"),
		DefaultValue("id")]
		
		public string Dt_guid
		{
			get
			{
				return dt_guid;
			}

			set
			{
				dt_guid = value;
			}
		}
		//传递参数的变量名称
		private string trans_guid;
		[Bindable(true),
		Category("data"),
		DefaultValue("id")]
		
		public string Trans_guid
		{
			get
			{
				return trans_guid;
			}

			set
			{
				trans_guid = value;
			}
		}

		//传递页码的变量名称
		private string trans_page;
		[Bindable(true),
		Category("data"),
		DefaultValue("")]
		
		public string Trans_page
		{
			get
			{
				return trans_page;
			}

			set
			{
				trans_page = value;
			}
		}


	

		#endregion 表格数据内容属性

		#region 表格外观控制属性
		
		

		//是否在表格中加checkbox
		private bool chborNo;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool ChborNo
		{
			get
			{
				return chborNo;
			
			}

			set
			{
				chborNo = value;
				
			}
		}

        //执行删除指定记录的页面
		private string delepage;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string Delepage
		{
			get
			{
				return delepage;
			}

			set
			{
				delepage = value;
			}
		}



		//是否在表格中加链接
		private bool linkorNo;
		[Bindable(true),
		Category("表格"),
		DefaultValue("true")]
		
		public bool LinkorNo
		{
			get
			{
				return linkorNo;
			
			}

			set
			{
				linkorNo = value;
				
			}
		}

		//链接打开方式设定，t1是指定参数方法打开，t2是直接打开指定页
		private string linktype;
		[Bindable(true),
		Category("表格"),
		DefaultValue("t1")]
		
		public string LinkType
		{
			get
			{
				return linktype;
			
			}

			set
			{
				linktype = value;
				
			}
		}

		//指定显示或编辑数据的页面
		private string linkpage;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string Linkpage
		{
			get
			{
				return linkpage;
			}

			set
			{
				linkpage = value;
			}
		}
	 /*----------------------------------------附加列属性设置开始-------------------------------------------------------*/

		
		/*-----------------------------附加列1*/
		//是否在表格中加附加列1
		private bool linkAdd1;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd1
		{
			get
			{
				return linkAdd1;
			
			}

			set
			{
				linkAdd1 = value;
				
			}
		}

		//附加列1单元格显示文字
		private string la1Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La1Text
		{
			get
			{
				return la1Text;
			
			}

			set
			{
				la1Text = value;
				
			}
		}

		//附加列1连接的页面
		private string la1Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La1Page 
		{
			get
			{
				return la1Page;
			}

			set
			{
				la1Page = value;
			}
		}

		/*-----------------------------附加列2*/

		//是否在表格中加附加列2
		private bool linkAdd2;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd2
		{
			get
			{
				return linkAdd2;
			
			}

			set
			{
				linkAdd2 = value;
				
			}
		}

		//附加列2单元格显示文字
		private string la2Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La2Text
		{
			get
			{
				return la2Text;
			
			}

			set
			{
				la2Text = value;
				
			}
		}

		//附加列2连接的页面
		private string la2Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La2Page 
		{
			get
			{
				return la2Page;
			}

			set
			{
				la2Page = value;
			}
		}

		
		/*-----------------------------附加列3*/
		//是否在表格中加附加列3
		private bool linkAdd3;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd3
		{
			get
			{
				return linkAdd3;
			
			}

			set
			{
				linkAdd3 = value;
				
			}
		}

		//附加列3单元格显示文字
		private string la3Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La3Text
		{
			get
			{
				return la3Text;
			
			}

			set
			{
				la3Text = value;
				
			}
		}

		//附加列3连接的页面
		private string la3Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La3Page 
		{
			get
			{
				return la3Page;
			}

			set
			{
				la3Page = value;
			}
		}

		/*-----------------------------附加列4*/

		//是否在表格中加附加列4
		private bool linkAdd4;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd4
		{
			get
			{
				return linkAdd4;
			
			}

			set
			{
				linkAdd4 = value;
				
			}
		}

		//附加列4单元格显示文字
		private string la4Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La4Text
		{
			get
			{
				return la4Text;
			
			}

			set
			{
				la4Text = value;
				
			}
		}

		//附加列4连接的页面
		private string la4Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La4Page 
		{
			get
			{
				return la4Page;
			}

			set
			{
				la4Page = value;
			}
		}

		/*-----------------------------附加列5*/
		//是否在表格中加附加列5
		private bool linkAdd5;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd5
		{
			get
			{
				return linkAdd5;
			
			}

			set
			{
				linkAdd5 = value;
				
			}
		}

		//附加列5单元格显示文字
		private string la5Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La5Text
		{
			get
			{
				return la5Text;
			
			}

			set
			{
				la5Text = value;
				
			}
		}

		//附加列5连接的页面
		private string la5Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La5Page 
		{
			get
			{
				return la5Page;
			}

			set
			{
				la5Page = value;
			}
		}

		/*-----------------------------附加列6*/

		//是否在表格中加附加列6
		private bool linkAdd6;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool LinkAdd6
		{
			get
			{
				return linkAdd6;
			
			}

			set
			{
				linkAdd6 = value;
				
			}
		}

		//附加列6单元格显示文字
		private string la6Text;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public string La6Text
		{
			get
			{
				return la6Text;
			
			}

			set
			{
				la6Text = value;
				
			}
		}

		//附加列6连接的页面
		private string la6Page ;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string La6Page 
		{
			get
			{
				return la6Page;
			}

			set
			{
				la6Page = value;
			}
		}
		
     /*----------------------------------------附加列属性设置结束-------------------------------------------------------*/
	
		//列数参数
		private int columnum;

		[Bindable(true),
		Category("表格"),
		DefaultValue("3")]
		public int Columnum
		{
			get
			{
				return columnum;
			}

			set
			{
				columnum = value;
			}
		}
		
		//行高
		private int tdHeight;

		[Bindable(true),
		Category("表格"),
		DefaultValue("30")]
		public int TdHeight
		{
			get
			{
				return tdHeight;
			}

			set
			{
				tdHeight = value;
			}
		}
	





	    //表内容背景参数
		private string trbgcolor;

		[Bindable(true),
		Category("表格"),
		DefaultValue("#ffffff")]
		public string Trbgcolor
		{
			get
			{
				return trbgcolor;
			}

			set
			{
				trbgcolor = value;
			}
		}

	
		//导航栏背景参数
		private string nag_trbgcolor;

		[Bindable(true),
		Category("导航栏"),
		DefaultValue("")]
		public string nag_Trbgcolor
		{
			get
			{
				return nag_trbgcolor;
			}

			set
			{
				nag_trbgcolor = value;
			}
		}


		//是否在表格中加onmouseover颜色标识
		private bool colortrorNo;
		[Bindable(true),
		Category("表格"),
		DefaultValue("false")]
		
		public bool ColortrorNo
		{
			get
			{
				return colortrorNo;
			
			}

			set
			{
				colortrorNo = value;
				
			}
		}


		//表内行当鼠标over时的背景参数，如#123456
		private string trovercolor;

		[Bindable(true),
		Category("表格"),
		DefaultValue("")]
		public string Trovercolor
		{
			get
			{
				return trovercolor;
			}

			set
			{
				trovercolor = value;
			}
		}

		#endregion 表格外观控制属性

		#region 中间过程控制变量
		//开始记录
		
		public int nStartcount
		{
			get
			{
				if (ViewState["nStartcount"] == null)
					return 0;
				return (int)ViewState["nStartcount"];
			}
			set
			{
				ViewState["nStartcount"] = value;
			}
		}
		//当前页，不是真正的页码，是在本组页码的第几页
		public int CurrentPageIndex
		{
			get
			{
				if (ViewState["CurrentPageIndex"] == null)
					return 1;
				return (int)ViewState["CurrentPageIndex"];
			}
			set
			{
				ViewState["CurrentPageIndex"] = value;
			}
		}
		
		//当前实际页码
		public int CurrentPageNo
		{
			get
			{
				if (ViewState["CurrentPageNo"] == null)
					return 1;
				return (int)ViewState["CurrentPageNo"];
			}
			set
			{
				ViewState["CurrentPageNo"] = value;
			}
		}
		//表示第几组页码
		public int CurrentTab
		{
	
			get
			{
				if (ViewState["CurrentTab"] == null)
					return 0;
				return (int)ViewState["CurrentTab"];
			}
			set
			{
				ViewState["CurrentTab"] = value;
			}     
	   
		}

		#endregion  中间过程控制变量
	
		protected override void CreateChildControls() 
		{       
			
		
			nTotalCount=Dtbname.Rows.Count;
			if(nTotalCount>0)
			{
			Controls.Clear();
			#region 在onPreRender中创建表格的知识点
			//以下创建表格行

			/*int nEndcount;
			if(nStartcount+PageSize>=dtbname.Rows.Count)
			{
				nEndcount=dtbname.Rows.Count;}
			else
			{
				nEndcount=nStartcount+PageSize;
			}
			
			HyperLink[] dtkey=new HyperLink[dtbname.Rows.Count+2];
			for (int i=nStartcount; i < nEndcount; i++)
			{
			
				trcontent =new TableRow();
				Controls.Add(trcontent);
				trcontent.BackColor=System.Drawing.ColorTranslator.FromHtml(Trbgcolor);
				//第一列加连接
				TableCell tc = new TableCell();
				trcontent.Cells.Add(tc);
				dtkey[i]=new HyperLink();
				dtkey[i].Text=dtbname.Rows[i][Dt_guid].ToString();
				dtkey[i].NavigateUrl=Linkpage+"?"+Trans_guid+"="+Dt_guid;
				tc.Controls.Add(dtkey[i]);
				

				for (int j=1; j < Columnum; j++)
				{
					TableCell tco = new TableCell();
					if (dtbname.Rows[i][j].ToString()=="")
						tco.Text="&nbsp;";
					else	tco.Text=dtbname.Rows[i][j].ToString();
					trcontent.Cells.Add(tco);
 
				}
			}




              

			//以下创建linkbutton	
			//nagrow = new TableRow();
			//Controls.Add(nagrow);
			//nagrow.BorderWidth=0;
		*/

			#endregion
			//创建导航单元格
			nagcell = new TableCell();
			nagcell.BorderWidth=0;
			nagcell.HorizontalAlign=HorizontalAlign.Right;
			Controls.Add(nagcell);

			/*
			//创建checkbox单元格
			boxcell = new TableCell();
			boxcell.BorderWidth=0;
			boxcell.HorizontalAlign=HorizontalAlign.Right;
			Controls.Add(boxcell);
			

			//创建checkbox按钮
            cbevery=new CheckBox();
			


			//创建保存为excel的按钮
			saveasxls= new LinkButton();
			saveasxls.ID="saveasxls";
			saveasxls.Text=".xls";
			saveasxls.ToolTip="保存为excel文件";
 
             
			saveasxls.Click += new EventHandler(this.saveasxlsClicked);

			nagcell.Controls.Add(saveasxls);
			nagcell.Controls.Add(new LiteralControl("&nbsp;"));
			
			*/

			//计算页数
			
				nTotalPage = nTotalCount/PageSize; 
				nTotalPage += nTotalCount % PageSize > 0? 1:0;
			
				if(DispPageCount>nTotalPage)
					nEndPage=nTotalPage;
				else
					nEndPage=DispPageCount;

		
			
			
				//显示上DispPageCount页
				lbPrevn = new LinkButton();
				lbPrevn.ID="Prevn";
				lbPrevn.Text="<<";
				lbPrevn.ToolTip="上"+DispPageCount.ToString()+"页";
				lbPrevn.Click += new EventHandler(this.Prev_nButtonClicked);
				nagcell.Controls.Add(lbPrevn);

				nagcell.Controls.Add(new LiteralControl("&nbsp;"));
		


			
				/*和上n页按钮不能同时使用
				 //上一页按钮

				lbPrev = new LinkButton();
				lbPrev.ID = "lbPrev";
				lbPrev.Text = "<";
				lbPrev.ToolTip="上一页";
				lbPrev.Click += new EventHandler(this.PreviousButtonClicked);
				nagcell.Controls.Add(lbPrev);

				nagcell.Controls.Add(new LiteralControl("&nbsp;"));
				*/
			
			
			
				//数字页码
				GoPage=new LinkButton[nTotalPage+2];
				for(int n=1;n<=nEndPage;n++)
				{
				
				
					GoPage[n]=new LinkButton();
					//GoPage[n].Text="["+n.ToString()+"]";
				

				
					GoPage[n].ID="GoPage"+n.ToString();
					GoPage[n].Click+=new EventHandler(this.Gopageclicked);
					nagcell.Controls.Add(GoPage[n]);
					nagcell.Controls.Add(new LiteralControl("&nbsp;"));
                
			

				}
           
				GoPage[CurrentPageIndex].Enabled=true;
       


				/*和下n页按钮不能同时使用
				//下一页按钮
				lbNext = new LinkButton();
				lbNext.ID = "Next";
				lbNext.Text = ">";
				lbNext.ToolTip="下一页";
				lbNext.Click += new EventHandler(this.NextButtonClicked);
				nagcell.Controls.Add(lbNext);

				nagcell.Controls.Add(new LiteralControl("&nbsp;"));
		
		
				lbPrev.Visible=false;
				lbNext.Visible =true;
				 */



				//显示下DispPageCount页按钮
			
				lbNextn = new LinkButton();
				lbNextn.ID="Nextn";
				lbNextn.Text=">>";
				lbNextn.ToolTip="下"+DispPageCount.ToString()+"页";
				lbNextn.Click += new EventHandler(this.Next_nButtonClicked);
				nagcell.Controls.Add(lbNextn);


				nagcell.Controls.Add(new LiteralControl("&nbsp;"));
			
			}
			
			
			//最后一页按钮

		
		}

		
		#region 相关方法
		/*
		//存储为excel文件事件方法，在控件中调用有问题，改用单独方法，调用方式相同
		private void saveasxlsClicked(Object sender, EventArgs e)  
		{
			
			dt2excel toexcel =new dt2excel();
			toexcel.do_dt2excel(Dtbname,"result",thd,Dir_xls);
			
		}
		
		不能和上下n页事件方法同时使用
		//上一页事件方法
		private void PreviousButtonClicked(Object sender, EventArgs e)
		{
			if (CurrentPageIndex >0)
				CurrentPageIndex--;
                nStartcount=CurrentPageIndex*PageSize;
			    lbPrev.Visible = CurrentPageIndex != 0;
		}

		//下一页事件方法
		private void NextButtonClicked(Object sender, EventArgs e)
		{
			if (CurrentPageIndex < nTotalPage )
				CurrentPageIndex=CurrentPageIndex+1;
			    nStartcount=CurrentPageIndex*PageSize;
		    	lbPrev.Visible = CurrentPageIndex != 0;
			    lbNext.Visible = CurrentPageIndex < nTotalPage - 1 ;
		}
		*/

		//...：上DispPageCount页事件方法
		private void Prev_nButtonClicked(Object sender, EventArgs e)
		{
			CurrentTab--;
				            
			CurrentPageIndex=1;

			CurrentPageNo=CurrentTab*DispPageCount+CurrentPageIndex;
			nStartcount=(CurrentPageNo-1)*PageSize;

		
		}
		
		//...：下DispPageCount页事件方法
	
		private void Next_nButtonClicked(Object sender, EventArgs e)
		{
			
			CurrentTab++;
				
			CurrentPageIndex=1;

			CurrentPageNo=CurrentTab*DispPageCount+CurrentPageIndex;
			nStartcount=(CurrentPageNo-1)*PageSize;
			//lbPrev.Visible = CurrentPageIndex != 0;
			//lbNextn.Visible = DispPageCount < nTotalPage - 1 ;
		}
		//特定页事件方法
		private void Gopageclicked(Object sender, EventArgs e)
		{
			//HttpContext.Current.Response.Write("<script>alert('"+CurrentPageIndex+"')</script>");
						
			LinkButton lb = (LinkButton)sender;
			
			CurrentPageIndex=System.Convert.ToInt16(lb.ID.Substring(6));

			CurrentPageNo=CurrentTab*DispPageCount+CurrentPageIndex;
			nStartcount=(CurrentPageNo-1)*PageSize;
								
		    //lb.ForeColor=System.Drawing.ColorTranslator.FromHtml("#808080"); 
		
		}

		#endregion 相关方法
		protected override void OnPreRender(System.EventArgs e)
		{
						
			/*该部分应该同时放在相关方法中，以便在执行换页操作时在Page_PreRender中获取正确的值
			原文：Page_PreRender runs after all the click events, so you will get the right value after Gopageclicked is called
             */
		
			
			CurrentPageNo=CurrentTab*DispPageCount+CurrentPageIndex;
			nStartcount=(CurrentPageNo-1)*PageSize;
            /*结束
			HttpContext.Current.Response.Write("<script>alert('"+nStartcount+"')</script>");
			*/
			if(nTotalCount>0)
			{
				base.OnPreRender (e);

			
				lbPrevn.Visible=CurrentTab!=0;

				for(int n=1;n<=nEndPage;n++)
				{
				
				
					int nPageNumber = CurrentTab*DispPageCount + n;


					GoPage[n].Text="["+nPageNumber.ToString()+"]";
					GoPage[n].Visible=nPageNumber<=nTotalPage;
				
			

				}
				GoPage[CurrentPageIndex].Enabled=false;
				lbNextn.Visible=(CurrentTab+1)*DispPageCount<nTotalPage;		
			}	

		}

	  

		
		protected override void Render(HtmlTextWriter htw)
		{
						
			AddAttributesToRender(htw);
			//删除按钮相关函数
			if(ChborNo==true)
			{
				htw.Write("<script language=javascript>");
				
				htw.Write("function ken_checkAll(obj)");
				htw.Write("{");
				htw.Write(" var inputs = document.forms[0].elements;");
				htw.Write(" for (var i=0; i < inputs.length; i++)");
				htw.Write("   if (inputs[i].type == 'checkbox' && inputs[i] != obj)");
				htw.Write("	inputs[i].checked = obj.checked;");
				htw.Write("}");
				
				htw.Write("</script>");




				htw.Write("<script language=javascript>");

				htw.Write("function ken_getChecked(obj)");
				htw.Write("{");
				htw.Write("  var inputs = document.forms[0].elements;");
				htw.Write("  var values = new Array();");
				htw.Write("  for (var i=0; i < inputs.length; i++)");
               	htw.Write("   if (inputs[i].type == 'checkbox' && inputs[i].checked && inputs[i].id !='del_adel1box')");
				htw.Write("	    values.push(inputs[i].value);");//将选中的chkbox加入values数组
				
				
				htw.Write("if (values.length == 0)");
				htw.Write("{");
				htw.Write("alert('请选择要删除的记录！');");
				htw.Write("	return false;");
				htw.Write("}");
				htw.Write("else");
                htw.Write("{");
				htw.Write("	if (confirm('你确定删除'+values.length+'条记录么?'))");
				 htw.Write("{");
				htw.Write(" obj.href += values.join(',');");//**将数组元素用以“,”分开的字符串返回
				htw.Write("  return true;");
				htw.Write("}");
				htw.Write("else");
			    htw.Write("   return false;");
				
				htw.Write("}");
				//htw.Write(" return confirm('确实要删除吗?')");
				htw.Write("}");

			

				htw.Write("</script>");

			}
			if(LinkAdd2==true||LinkAdd4==true||LinkAdd6==true)
			{
				htw.Write("<script language=javascript>");
				htw.Write("function WinOpen(sUrl)");
				htw.Write("{");						
				htw.Write("window.open(sUrl,'','top=0,left=0,width='+Math.ceil(window.screen.width-10)+',height='+Math.ceil(window.screen.height-78)+',toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, status=yes')");
				htw.Write("}");
				htw.Write("</script>");
			}
				//htw.AddAttribute(HtmlTextWriterAttribute.Bordercolor,Bordercolor);
				htw.AddAttribute(HtmlTextWriterAttribute.Cellpadding,"0");
				htw.AddAttribute(HtmlTextWriterAttribute.Cellspacing,"0");
				//htw.RenderBeginTag(HtmlTextWriterTag.Table);
			
				//nStartcount=CurrentPageIndex*PageSize+StartIndex;
			if(nTotalCount>0)
			{
				int nEndcount;
				if(nStartcount+PageSize>=Dtbname.Rows.Count)
				{
					nEndcount=Dtbname.Rows.Count;
				}
				else
				{
					nEndcount=nStartcount+PageSize;
				}

                if(Trbgcolor=="")
					Trbgcolor="#ffffff";
				
				#region  开始写数据表格
				
				for (int i=nStartcount; i < nEndcount; i++)
				{
					//每行加颜色标记
					
					if(ColortrorNo==true)
					{
                      htw.Write("<tr bgcolor='"+Trbgcolor+"' onmouseover=this.style.backgroundColor='"+Trovercolor+"' onmouseout=this.style.backgroundColor='"+Trbgcolor+"'>");
					}
					else
					{
						htw.Write("<tr  bgcolor='"+Trbgcolor+"'>");
					}
					
					//是否加入checkbox列
					if(ChborNo==true)
					{
						htw.Write("<td height="+TdHeight+">");
						htw.Write("<INPUT  type='checkbox'	name='checked' value='"+Dtbname.Rows[i][Dt_guid].ToString()+"'>");
						//htw.Write("<INPUT  type='checkbox'	name='checked' value='66'>");

						htw.Write("</td>");
					}
					


					htw.Write("<td height="+TdHeight+">");
					//第一列加连接
					if (LinkorNo==true)
					{
					
						if(LinkType=="t2")
						{
							htw.Write("<a href="+Dtbname.Rows[i][Dt_guid].ToString()+" >");
							htw.Write(Dtbname.Rows[i][0].ToString());
							htw.Write("</a>");
						}
						else
						{
							htw.Write("<a title='显示/编辑详细内容' href="+Linkpage+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
							htw.Write(Dtbname.Rows[i][0].ToString());
							htw.Write("</a>");
						}

					}
					else
					{
						htw.Write(Dtbname.Rows[i][0].ToString());
				
					}
					htw.Write("</td>");


					for (int j=1; j < Columnum; j++)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						if (Dtbname.Rows[i][j].ToString()=="")
							htw.Write("&nbsp;");
						else htw.Write(Dtbname.Rows[i][j].ToString());

						htw.Write("</td>");
 
					}
                    
					//加入附加列1
					if (LinkAdd1==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						htw.Write("<a href="+La1Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write(La1Text);
						htw.Write("</a>");
						htw.Write("</td>");
                    }

					//加入附加列2
					if (LinkAdd2==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						//htw.Write("<a href="+La2Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write("<a href=# onclick=\"WinOpen(\'"+La2Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+"\')\">");
						//htw.Write("<a href=# onclick=\"WinOpen(\'http://10.67.152.8/index76.asp\')\">");
						htw.Write(La2Text);
						htw.Write("</a>");
						htw.Write("</td>");
					}
					//加入附加列3
					if (LinkAdd3==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						htw.Write("<a href="+La3Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write(La3Text);
						htw.Write("</a>");
						htw.Write("</td>");
					}

					//加入附加列4
					if (LinkAdd4==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						//htw.Write("<a href="+La4Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write("<a href=# onclick=\"WinOpen(\'"+La4Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+"\')\">");
						//htw.Write("<a href=# onclick=\"WinOpen(\'http://10.67.154.8/index76.asp\')\">");
						htw.Write(La4Text);
						htw.Write("</a>");
						htw.Write("</td>");
					}
					//加入附加列5
					if (LinkAdd5==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						htw.Write("<a href="+La5Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write(La5Text);
						htw.Write("</a>");
						htw.Write("</td>");
					}

					//加入附加列6
					if (LinkAdd6==true)
					{
						htw.Write("<td align=center height="+TdHeight+">");
						//htw.Write("<a href="+La6Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+" >");
						htw.Write("<a href=# onclick=\"WinOpen(\'"+La6Page+"?"+Trans_guid+"="+Dtbname.Rows[i][Dt_guid].ToString()+"&"+Trans_page+"="+CurrentPageNo+"\')\">");
						//htw.Write("<a href=# onclick=\"WinOpen(\'http://10.67.156.8/index76.asp\')\">");
						htw.Write(La6Text);
						htw.Write("</a>");
						htw.Write("</td>");
					}
					htw.Write("</tr>");

				}
				
            
				htw.Write("</table>");

				#endregion 写数据表格结束
				//border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2" bgcolor="#00FFFF"
				htw.Write("<table border=0 cellpadding=0 cellspacing=0 width=100% >");
				
				//加入全选、取消全选及删除选定记录功能行
				if(ChborNo==true)
				{
					htw.Write("<tr width=100% bgcolor="+nag_Trbgcolor+">");
					htw.Write("<td align=left>");
					htw.Write("<INPUT  id='del_adel1box' type='checkbox' name='checkall' onclick='ken_checkAll(this)'>&nbsp;全选/取消全选 " );
					htw.Write("&nbsp;&nbsp;");
					
					htw.Write("<a id='del_adel1' href='"+Delepage +"?"+Trans_guid+"=' onclick='javascript:return ken_getChecked(this)'>");
					//htw.Write("<a id='del' href="+Delepage +"?"+Trans_guid+"="+" onclick='getChecked(this)'>");
				   // htw.Write("<a id='del' href="+Delepage+"?"+Trans_guid+"="+变量+">");' onclick='getChecked(this)'
					htw.Write("删除选定记录");
					htw.Write("</a>");
					htw.Write("</td>");
					htw.Write("</tr>");
				}
				


				

               
				htw.Write("<tr bgcolor="+nag_Trbgcolor+">");
				
				//将分页按钮等写入页面
				nagcell.RenderControl(htw);

				htw.Write("<td align=right width=100>");
				htw.Write("&nbsp;");
				htw.Write("&nbsp;");
				htw.Write("<a title='DataTable分页控件 2003-8fv 2005-5sv build on .net1.1--王震作品' href='mailto:ken@slof.com'> ..</a>");
				htw.Write("&nbsp;");
				htw.Write("&nbsp;");
				htw.Write("共");
				htw.Write(nTotalPage);
				htw.Write("页");
			
				htw.Write("</td>");
				htw.Write("</tr>");
			
		
				//htw.RenderEndTag();

			}
			else{
			
				htw.Write("<tr bgcolor="+nag_Trbgcolor+">");
				htw.Write("<td align=left width=100 colspan="+ Columnum+">");
				htw.Write("暂无数据");
				htw.Write("</td>");
				htw.Write("</tr>");
			}

		}
		
	}
}
